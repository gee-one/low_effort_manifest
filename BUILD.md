Low Effort
===========

This was tested on a linode VM using Debian 11.  The VM was a dedicated 96 gb with 48 cores, that cost $1.08/hr

Feel free to use my referral link- you get a $100 credit that is good for 60 days.  You do need a valid payment method to use it.

https://www.linode.com/lp/refer/?r=57d204df88f79498cb6a8f6749dec1896fd919e0

# install software
```
apt update
apt upgrade -V
```

```
apt install tmux nmon openssh-server -V
```

```
apt install bc bison build-essential ccache curl flex fontconfig g++-multilib gcc-multilib git git-core gnupg gperf \
imagemagick lib32ncurses5-dev lib32readline-dev lib32z1-dev libc6-dev-i386 libgl1-mesa-dev liblz4-tool libncurses5 \
libncurses5-dev libsdl1.2-dev libssl-dev libx11-dev libxml2 libxml2-utils lzop pngcrush python rsync schedtool squashfs-tools \
unzip x11proto-core-dev xsltproc zip zlib1g-dev -V
```

# install repo
```
export REPO=$(mktemp /tmp/repo.XXXXXXXXX)
curl -o ${REPO} https://storage.googleapis.com/git-repo-downloads/repo
gpg --recv-key 8BB9AD793E8E6153AF0F9A4416530D5E920F5C65
curl -s https://storage.googleapis.com/git-repo-downloads/repo.asc | gpg --verify - ${REPO} && install -m 755 ${REPO} /usr/local/bin/repo
```


# set up an unprivileged user
```
adduser <your name here>
adduser <your name here> sudo
```

# reboot and connect as un-priv user
```
ssh <your name here>@<VM IP address>
```

# if you use tmux, set up .tmux_conf
```
cat << EOF >> ~/.tmux.conf
set -g mouse on
set -g history-limit 200000
EOF
```

# set up git
```
git config --global user.email "<your email>"
git config --global user.name "<your name>"
```

# sync and build
```
mkdir low_effort
cd low_effort

repo init -u https://gitlab.com/gee-one/low_effort_manifest.git -b android-13.0
repo sync -j 20

. build/envsetup.sh
export DISABLE_ARTIFACT_PATH_REQUIREMENTS=true

lunch aosp_bluejay-userdebug
m vendorbootimage otapackage

lunch aosp_oriole-userdebug
m vendorbootimage otapackage
```
