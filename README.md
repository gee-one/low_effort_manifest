Low Effort
===========

Low Effort is an AOSP rom with minimal changes.  A lot of the commits to get the basic functions working correctly are from Graphene OS.


Getting started
---------------

To initialize your local repository:
```
repo init -u https://gitlab.com/gee-one/low_effort_manifest.git -b android-13.0
```
Then to sync up:
```
repo sync
```

This is untested so there might be errors.

For more information,

[Overview](https://source.android.com/setup/start)

[Repo Init](https://source.android.com/setup/build/downloading)

[Building](https://source.android.com/setup/build/building)

These packages should be sufficient to build on Debian 11/Bullseye

```
apt install bc bison build-essential ccache curl flex fontconfig g++-multilib gcc-multilib git git-core gnupg gperf \
imagemagick lib32ncurses5-dev lib32readline-dev lib32z1-dev libc6-dev-i386 libgl1-mesa-dev liblz4-tool libncurses5 \
libncurses5-dev libsdl1.2-dev libssl-dev libx11-dev libxml2 libxml2-utils lzop pngcrush python rsync schedtool squashfs-tools \
unzip x11proto-core-dev xsltproc zip zlib1g-dev -V
```
